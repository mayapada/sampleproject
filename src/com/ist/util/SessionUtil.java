package com.ist.util;

import com.konylabs.middleware.session.Session;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class SessionUtil {


	public static Map<String, Object> sessionMap(Session session){
		if(session == null) return new HashMap<>();
		Map<String, Object> map = new HashMap<>();
		Enumeration enumeration = session.getAttributeNames();
		for(Object key : Collections.list(enumeration)){
			map.put((String) key, session.getAttribute((String) key));
		}
		return map;
	}

}
