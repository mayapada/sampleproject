package com.ist.util;

import com.google.gson.*;
import org.apache.log4j.Logger;

public final class JsonBuilder {
	
	private static Logger LOGGER = Logger.getLogger(JsonBuilder.class);
	
	/**
	 * 
	 */
	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	public static final Gson GSON_INCLUDE_NULL = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

	/**
	 * 
	 */
	private JsonBuilder() { }
	
	/**
	 * 
	 * @param json
	 * @param clazz
	 * @return
	 */
	public static <E> E fromJson(final String json, final Class<E> clazz) {
		E ret = (E) null;
		try {
			if (null == json) {
				throw new JsonParseException("Null json string");
			}
			
			final String _json = json.trim();
			if (_json.length() < 1) {
				throw new JsonParseException("Zero length json string");
			}
			
			if (_json.charAt(0) != '{' || !_json.endsWith("}")) {
				throw new JsonParseException("Invalid json format");
			}
			
			ret = JsonBuilder.GSON.fromJson(json, clazz);
		} catch (final Throwable t) {
//			t.printStackTrace();
			LOGGER.error("### Error parse from json : ", t);
		}
		
		return ret;
	}

	/**
	 * 
	 * @param object
	 * @return
	 */
	public static String toJson(final Object object) {
		String ret = (String) null;
		try {
			ret = JsonBuilder.GSON.toJson(object);
		} catch (final Throwable t) {
			LOGGER.error("### Error parse to json : ", t);
		}
		
		return ret;
	}

	/**
	 * 
	 * @param object
	 * @return
	 */
	public static <E> String toJsonIncludeNull(final Object object) {
		String ret = (String) null;
		try {
			ret = JsonBuilder.GSON_INCLUDE_NULL.toJson(object);
		} catch (final Throwable t) {
//			t.printStackTrace();
			LOGGER.error("### Error parse to json : ", t);
		}
		
		return ret;
	}
	
	/**
	 * 
	 * @param json
	 * @return
	 */
	public static JsonArray stringToJsonArray(final String json) {
		JsonArray jsonArr = (JsonArray) null;
		try {
			JsonParser parser = new JsonParser();
			JsonElement jsonElement = parser.parse(json);
			jsonArr = jsonElement.getAsJsonArray();
		} catch (final Throwable t) {
			LOGGER.error("### Error parse to jsonArray : ", t);
		}
		
		return jsonArr;
	}
	
}
