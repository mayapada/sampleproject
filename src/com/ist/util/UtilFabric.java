package com.ist.util;

import com.konylabs.middleware.api.OperationData;
import com.konylabs.middleware.api.ServiceRequest;
import com.konylabs.middleware.api.ServicesManager;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.exceptions.MiddlewareException;
import com.konylabs.middleware.registry.AppRegistryException;
import com.konylabs.middleware.session.Session;
import org.apache.log4j.Logger;

import java.util.HashMap;

public class UtilFabric {

	private static final Logger logger = Logger.getLogger(UtilFabric.class);


	public static String invokeService(HashMap requestMap, Session session, DataControllerRequest dcReq) {

		logger.debug("Invoke Service call :" + requestMap.get("serviceId") + "/"
				+ requestMap.get("operationId"));

		String serviceId = (String) requestMap.get("serviceId");
		String operationId = (String) requestMap.get("operationId");

		String version = (String) requestMap.getOrDefault("version", "1.0");

		try {
			ServicesManager servicesManager = dcReq.getServicesManager();
			OperationData serviceData = servicesManager
					.getOperationDataBuilder()
					.withServiceId(serviceId)
					.withOperationId(operationId)
					.withVersion(version)
					.build();

			ServiceRequest serviceRequest = servicesManager.getRequestBuilder (serviceData)
					.withInputs(requestMap)
					.withSessionMap(SessionUtil.sessionMap(session))
					.withDCRRequest(dcReq)
					.build();

			return serviceRequest.invokeServiceAndGetJson();
		} catch (AppRegistryException e) {
			logger.error("AppRegistryException : ",e);
		} catch (MiddlewareException e) {
			logger.error("MiddlewareException : ",e);
		}

		return null;
	}

}